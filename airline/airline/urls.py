"""airline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from django.views.generic import TemplateView

from .views import (
    BookingDetailView,
    BookingsView,
    NewBookingPaymentView,
    NewBookingView,
    RegisterView,
    TravellerView,
    TripsView,
)

urlpatterns = [
    # fmt: off
    path("login/", LoginView.as_view(template_name="airline/login.html"), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("register/", RegisterView.as_view(), name="register"),
    path("traveller/", TravellerView.as_view(), name="traveller"),
    path("book/", NewBookingView.as_view(), name="new-booking"),
    path("booking/<pk>/payment/", NewBookingPaymentView.as_view(), name="new-booking-payment"),
    path("booking/<pk>/", BookingDetailView.as_view(), name="booking"),
    path("bookings/", BookingsView.as_view(), name="bookings"),
    path("trips/", TripsView.as_view(), name="trips"),
    path("", TemplateView.as_view(template_name="airline/index.html"), name="index"),
    # fmt: on
]
