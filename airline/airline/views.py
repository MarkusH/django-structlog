import structlog

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import models, transaction
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, DetailView, FormView, ListView
from requests.exceptions import HTTPError

from .forms import NewBookingForm, NewBookingPaymentForm, RegisterForm
from .models import Booking, Traveller
from .payment import PaymentClient

logger = structlog.get_logger("structlog")


class TravellerView(LoginRequiredMixin, DetailView):
    template_name = "airline/traveller.html"

    def get_context_data(self, **kwargs):
        next_trip_booking = (
            Booking.objects.filter(
                departure_date__gte=timezone.now(), travellers__id=self.request.user.pk
            )
            .select_related("flight")
            .first()
        )
        return super().get_context_data(next_trip=next_trip_booking)

    def get_object(self):
        return self.request.user


class RegisterView(CreateView):
    model = Traveller
    form_class = RegisterForm
    template_name = "airline/register.html"
    success_url = reverse_lazy("traveller")


class BookingDetailView(LoginRequiredMixin, DetailView):
    model = Booking
    template_name = "airline/booking.html"

    def get_queryset(self):
        qs = super().get_queryset()
        user = self.request.user
        # fmt: off
        q = (
            models.Q(buyer=user)
            | models.Q(travellers=user, payment_status=Booking.PAYMENT_DONE)
        )
        # fmt: on
        return (
            qs.filter(q)
            .select_related("flight", "buyer")
            .prefetch_related("travellers")
            .order_by("departure_date")
        )


class BookingsView(LoginRequiredMixin, ListView):
    model = Booking
    template_name = "airline/bookings.html"

    def get_queryset(self):
        qs = super().get_queryset()
        qs = (
            qs.filter(buyer=self.request.user)
            .annotate(
                order=models.Case(
                    models.When(
                        then=models.Value(0), payment_status=Booking.PAYMENT_DONE
                    ),
                    models.When(
                        then=models.Value(1), payment_status=Booking.PAYMENT_PENDING
                    ),
                    output_field=models.SmallIntegerField(),
                )
            )
            .select_related("flight")
            .order_by("order", "departure_date")
        )
        return qs


class TripsView(LoginRequiredMixin, ListView):
    model = Booking
    template_name = "airline/trips.html"

    def get_queryset(self):
        user = self.request.user
        # fmt: off
        q = (
            models.Q(buyer=user)
            | models.Q(travellers=user, payment_status=Booking.PAYMENT_DONE)
        )
        # fmt: on
        return Booking.objects.filter(q).select_related("flight")


class NewBookingView(LoginRequiredMixin, CreateView):
    model = Booking
    form_class = NewBookingForm
    template_name = "airline/new_booking.html"

    def get_success_url(self):
        return reverse("new-booking-payment", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        obj = form.instance
        obj.buyer = self.request.user
        return super().form_valid(form)


class NewBookingPaymentView(LoginRequiredMixin, FormView):
    form_class = NewBookingPaymentForm
    template_name = "airline/new_booking_payment.html"
    success_url = reverse_lazy("traveller")

    def get_object(self, *, for_update=False):
        qs = Booking.objects.filter(
            payment_status=Booking.PAYMENT_PENDING
        ).select_related("flight")
        if for_update:
            qs = qs.select_for_update()
        return qs.get(pk=self.kwargs["pk"])

    def get_context_data(self, **kwargs):
        return super().get_context_data(booking=self.get_object())

    def form_valid(self, form):
        with structlog.threadlocal.tmp_bind(logger, booking_id=self.kwargs["pk"]):
            try:
                logger.info("payment_starting")
                with transaction.atomic():
                    obj = self.get_object(for_update=True)
                    obj.payment_status = Booking.PAYMENT_DONE
                    obj.save(update_fields=["payment_status"])
                    payment_client = PaymentClient(obj, self.request.trace_id)
                    payment_client.process(**form.cleaned_data)
            except HTTPError:
                form.add_error(None, "Error processing payment!")
                return super().form_invalid(form)

            logger.info("payment_finished")
            return super().form_valid(form)
